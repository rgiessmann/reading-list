# Reading List

contains impressive quotes from things I read.

## on incentives in academia

from https://doi.org/10.1089/ees.2016.0223 (references removed for ease of reading):

While there is virtually no research exploring the impact of perverse incentives on scientific productivity, most in academia would acknowledge a collective shift in our behavior over the years, emphasizing quantity at the expense of quality. This issue may be especially troubling for attracting and retaining altruistically minded students, particularly women and underrepresented minorities (WURM), in STEM research careers. Because modern scientific careers are perceived as focusing on “the individual scientist and individual achievement” rather than altruistic goals, and WURM students tend to be attracted toward STEM fields for altruistic motives, including serving society and one's community, many leave STEM to seek careers and work that is more in keeping with their values.

Thus, another danger of overemphasizing output versus outcomes and quantity versus quality is creating a system that is a “perversion of natural selection,” which selectively weeds out ethical and altruistic actors, while selecting for academics who are more comfortable and responsive to perverse incentives from the point of entry.

---

EOF
